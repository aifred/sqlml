import re
from pathlib import Path

import numpy as np
from gensim import corpora, models
from gensim.corpora import Dictionary

corpus_file_path = './sqlToVect.mm'
dictionary_file_path = './sqlTokens.dict'


def tfidf_vector(data_vector):
    # Transformation of textVectors to vector representation - (1) to discover relationships between words and (2)
    # to make the document representation more compact
    corpus = corpora.MmCorpus(corpus_file_path)

    tfidf = models.TfidfModel(corpus)  # initialise a TF-IDF model

    return tfidf[data_vector], tfidf


def vectorize_text_to_bow(text_data):
    # convert text as vectors
    dictionary = load_dictionary()
    data_vector = [dictionary.doc2bow(text) for text in text_data]

    return data_vector


def load_dictionary():
    dictionary_file = Path(dictionary_file_path)
    if dictionary_file.is_file():
        return Dictionary.load_from_text(dictionary_file_path)
    else:
        return None


def update_dictionary(data):
    existing_dictionary = load_dictionary()
    if existing_dictionary is None:
        print("Creating a new dictionary from scratch . . .")
        updated_dictionary = Dictionary(data)
        existing_dictionary = updated_dictionary
        existing_dictionary.save_as_text(dictionary_file_path)
    else:
        print("Loading dictionary from file path . . .")
        existing_dictionary.add_documents(data)
    print(existing_dictionary)


def enrich_data_with_features(documents):
    feature_dict = {}
    boolean_based = []
    union_based = []
    comment_based = []
    continuation_based = []
    sleep_based = []
    for row in documents:
        boolean_based_result = re.findall(r'[0-9]+=[0-9]+', row)  # captures use of 1=1 or 1=2 etc.
        union_based_result = re.findall(r'(union)+', row)  # captures union
        comment_based_result = re.findall(r"[\']*[\-\-]+", row)  # captures '-- or --
        continuation_based_result = re.findall(r"[;]+", row)  # capture uses of semi-colon
        sleep_based_result = re.findall(r"sleep\S\(*|SLEEP\S\(*", row)  # captures use of sleep()

        boolean_based.append(len(boolean_based_result))
        union_based.append(len(union_based_result))
        comment_based.append(len(comment_based_result))
        continuation_based.append(len(continuation_based_result))
        sleep_based.append(len(sleep_based_result))
    feature_dict['booleanBased'] = boolean_based
    feature_dict['unionBased'] = union_based
    feature_dict['commentBased'] = comment_based
    feature_dict['continuationBased'] = continuation_based
    feature_dict['sleepBased'] = sleep_based
    boolean_np = np.array(boolean_based)
    union_np = np.array(union_based)
    comment_np = np.array(comment_based)
    continuation_np = np.array(continuation_based)
    sleep_np = np.array(sleep_based)
    # print("boolean-based shape: {}".format(boolean_np.shape))
    # print("union-based shape: {}".format(union_np.shape))
    # print("comment-based shape: {}".format(comment_np.shape))
    # print(feature_dict)
    return np.array([boolean_np, union_np, comment_np, continuation_np, sleep_np])
