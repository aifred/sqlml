import logging
from collections import Counter

import gensim
import numpy as np
import tensorflow as tf
from gensim import corpora

from Utils import tfidf_vector, load_dictionary, vectorize_text_to_bow, corpus_file_path, update_dictionary, \
    enrich_data_with_features

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
print("Tensorflow {} loaded".format(tf.__version__))

train_data_path = './sql-train-label.csv'
validate_data_path = './sql-validate-label.csv'
predict_data_path = './sql-predict.csv'

step_size = 600
batch_size = 80
shuffle_size = 30
learning_rate = 0.87
LABELS = {'real': 0, 'fake': 1}
saveModelAsText = False


def encode_labels(labels):
    encoded_labels = np.array([LABELS.get(sentiment) for sentiment in labels])

    return encoded_labels


def retrieve_data(path, has_header=True, has_label=True):
    with open(path) as f:
        documents = f.read().splitlines()[1:] if has_header else f.read().splitlines()

        sql_queries = [(','.join(x)) for x in list(sql.lower().split(',')[:-1] for sql in documents) if
                       x] if has_label else [sql for sql in documents]
        sql_split_by_words = [[word for word in (','.join(document.lower().split(',')[:-1])).split()] for document in
                              documents]
        labels = [document.lower().split(',')[-1] for document in documents] if has_label else None

        additional_features = enrich_data_with_features(sql_queries)

        return sql_split_by_words, additional_features, labels


def load_data():
    (t_data, t_additional, t_label) = retrieve_data(train_data_path)
    (v_data, v_additional, v_label) = retrieve_data(validate_data_path)
    (p_data, p_additional, p_label) = retrieve_data(predict_data_path, has_label=False)

    data_messages_lens = Counter([len(x) for x in t_data])
    validate_messages_lens = Counter([len(x) for x in v_data])
    predict_messages_lens = Counter([len(x) for x in p_data])
    print("[Train_Data] Maximum message length: {}".format(max(data_messages_lens)))
    print("[Train_Data] Average message length: {}".format(np.mean([len(x) for x in t_data])))
    print("[Validate_Data] Maximum message length: {}".format(max(validate_messages_lens)))
    print("[Validate_Data] Average message length: {}".format(np.mean([len(x) for x in v_data])))
    print("[Predict_Data] Maximum message length: {}".format(max(predict_messages_lens)))
    print("[Predict_Data] Average message length: {}".format(np.mean([len(x) for x in p_data])))

    update_dictionary(t_data)

    encoded_train_labels = encode_labels(t_label)
    encoded_validate_labels = encode_labels(v_label)

    print("[Train_Data] Data Shape: {}".format(np.array(t_data).shape))
    print("[Train_Data] Label Shape: {}".format(np.array(t_label).shape))
    print("[Validate_Data] Data Shape: {}".format(np.array(v_data).shape))
    print("[Validate_Data] Label Shape: {}".format(np.array(v_label).shape))
    print("[Predict_Data] Data Shape: {}".format(np.array(p_data).shape))

    dictionary = load_dictionary()
    # Vectorize text to vectors and store in local disk for future usage
    train_data_vector = vectorize_text_to_bow(t_data)
    validate_data_vector = vectorize_text_to_bow(v_data)
    predict_data_vector = vectorize_text_to_bow(p_data)

    print('\nAfter vectorization:')
    for doc in train_data_vector:
        print(doc)
    corpora.MmCorpus.serialize(corpus_file_path, train_data_vector)  # store to disk for future use

    # convert to TFIDF (Term Frequency - Inverse Document Frequency)
    (train_data_tfidf, train_tfidf) = tfidf_vector(train_data_vector)
    (validate_data_tfidf, validate_tfidf) = tfidf_vector(validate_data_vector)
    (predict_data_tfidf, predict_tfidf) = tfidf_vector(predict_data_vector)
    print("Train data tfidf length: {}".format(len(train_tfidf.dfs)))
    print("Validate data tfidf length: {}".format(len(validate_tfidf.dfs)))
    print("Predict data tfidf length: {}".format(len(predict_tfidf.dfs)))

    print('\nAfter vectorization with TF-IDF:')
    for doc in train_data_tfidf:
        print(doc)

    # Convert to numpy matrix for input to tensorflow
    train_data_numpy_matrix = gensim.matutils.corpus2dense(train_data_tfidf, num_terms=len(dictionary))
    validate_data_numpy_matrix = gensim.matutils.corpus2dense(validate_data_tfidf, num_terms=len(dictionary))
    predict_data_numpy_matrix = gensim.matutils.corpus2dense(predict_data_tfidf, num_terms=len(dictionary))

    train_data_with_features = np.vstack((train_data_numpy_matrix, t_additional))
    validate_data_with_features = np.vstack((validate_data_numpy_matrix, v_additional))
    predict_data_with_features = np.vstack((predict_data_numpy_matrix, p_additional))
    print('train_data shape: {}'.format(train_data_numpy_matrix.shape))
    print("train_additional shape: {}".format(t_additional.shape))
    print('train_label shape: {}'.format(encoded_train_labels.shape))
    print('train_with_feature shape: {}'.format(train_data_with_features.shape))
    print('validate_data shape: {}'.format(validate_data_numpy_matrix.shape))
    print("validate_additional shape: {}".format(v_additional.shape))
    print('validate_label shape: {}'.format(encoded_validate_labels.shape))
    print('validate_with_feature shape: {}'.format(validate_data_with_features.shape))
    print('predict_data_shape: {}'.format(predict_data_numpy_matrix.shape))
    print("predict_additional shape: {}".format(p_additional.shape))
    print('predict_with_feature shape: {}'.format(predict_data_with_features.shape))

    return train_data_with_features, encoded_train_labels, validate_data_with_features, encoded_validate_labels, \
           predict_data_with_features


def train_input_fn(features, labels, batch_size_arg):
    print('Input shape: {}'.format(features.shape))

    new_dict_feature = {}
    for key, value in enumerate(features):
        new_dict_feature[str(key)] = value

    # convert numpy to tf Dataset
    dataset = tf.data.Dataset.from_tensor_slices((new_dict_feature, labels))

    return dataset.shuffle(shuffle_size).repeat().batch(batch_size_arg)


def eval_input_fn(features, labels, batch_size_arg):
    """An input function for evaluation or prediction"""

    print('[Eval_Input_Fn] Feature Shape: {}'.format(features.shape))
    new_dict_feature = {}
    for key, value in enumerate(features):
        new_dict_feature[str(key)] = value

    if labels is None:
        inputs = new_dict_feature
    else:
        inputs = (new_dict_feature, labels)

    dataset = tf.data.Dataset.from_tensor_slices(inputs)

    assert batch_size_arg is not None, "batch_size must not be None"

    return dataset.batch(batch_size_arg)


def train_model(t_data, t_label, v_data, v_label, p_data):
    feature_columns = []
    for key in enumerate(t_data):
        feature_columns.append(tf.feature_column.numeric_column(key=str(key[0])))

    print('feature columns: {}'.format(feature_columns))

    # Create a high-level Estimator from the compiled Keras model.
    estimator_config = tf.estimator.RunConfig(
        keep_checkpoint_max=5
    )

    # Estimator using the default optimizer.
    classifier = tf.estimator.LinearClassifier(
        feature_columns=feature_columns,
        n_classes=2,
        optimizer=tf.train.GradientDescentOptimizer(
            learning_rate=learning_rate
        ),
        config=estimator_config)

    # Train the model
    classifier.train(input_fn=lambda: train_input_fn(t_data, t_label, batch_size), steps=step_size)

    # Evaluate the model
    eval_result = classifier.evaluate(input_fn=lambda: eval_input_fn(v_data, v_label, batch_size))
    print('\nTest set accuracy: {accuracy:0.3f}\n'.format(**eval_result))

    expected = []
    with open('sql-predict-expected.csv') as file:
        for line in file:
            expected.append(line.rstrip())

    new_feature_dict = {}
    for numeric_column in feature_columns:
        new_feature_dict[numeric_column.key] = tf.placeholder(dtype=numeric_column.dtype, shape=numeric_column.shape,
                                                              name=numeric_column.name)
    serving_input = tf.estimator.export.build_raw_serving_input_receiver_fn(new_feature_dict)

    # Save the model for future usage
    classifier.export_saved_model(export_dir_base="./models", serving_input_receiver_fn=serving_input,
                                  as_text=saveModelAsText)

    # Prediction model
    predictions = classifier.predict(input_fn=lambda: eval_input_fn(p_data, labels=None, batch_size_arg=batch_size))
    # template = ('\nPrediction is "{}" ({:.1f}%), expected "{}"')
    template = 'Prediction is "{}" ({:.1f}%), expected "{}"\n'

    reversed_labels = {}
    for key, value in LABELS.items():
        reversed_labels[value] = key

    for pred_dict, expec in zip(predictions, expected[1:]):
        print(pred_dict)

        class_id = pred_dict['class_ids'][0]
        probability = pred_dict['probabilities'][class_id]
        print(template.format(reversed_labels[class_id],
                              100 * probability, expec))


# create data pipeline
(train_data, train_label, validate_data, validate_label, predict_data) = load_data()

# train model with data
train_model(train_data, train_label, validate_data, validate_label, predict_data)
