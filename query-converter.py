import json
import logging

import gensim
import numpy as np
import requests
import tensorflow as tf

from Utils import tfidf_vector, load_dictionary, vectorize_text_to_bow, update_dictionary, enrich_data_with_features

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
print("Tensorflow {} loaded".format(tf.__version__))

train_data_path = './sql-train-label.csv'
validate_data_path = './sql-validate-label.csv'
predict_data_path = './sql-predict.csv'
step_size = 600
batch_size = 80
shuffle_size = 30
learning_rate = 0.87
include_predict_signature = True


def convert_query_to_bow(query):
    # Vectorize text to vectors and store in local disk for future usage
    additional_features = enrich_data_with_features([query])
    sql_split_by_words = [query.lower().split()]
    print(sql_split_by_words)

    update_dictionary(sql_split_by_words)
    dictionary = load_dictionary()
    query_data_vector = vectorize_text_to_bow(sql_split_by_words)
    print(query_data_vector)

    # convert to TFIDF (Term Frequency - Inverse Document Frequency)
    (query_data_tfidf, query_tfidf) = tfidf_vector(query_data_vector)
    print("Query data tfidf length: {}".format(len(query_tfidf.dfs)))
    print('\nAfter vectorization with TF-IDF:')
    for doc in query_data_tfidf:
        print(doc)
    print(query_tfidf)

    # Convert to numpy matrix for input to tensorflow
    query_data_numpy_matrix = gensim.matutils.corpus2dense(query_data_tfidf, num_terms=len(dictionary))
    print('query_data shape: {}'.format(query_data_numpy_matrix.shape))
    print("additional_features shape: {}".format(additional_features.shape))

    query_data_with_features = np.vstack((query_data_numpy_matrix, additional_features))
    print('query_with_feature shape: {}'.format(query_data_with_features.shape))
    query_list = query_data_with_features.tolist()
    input_map = {}
    for index, data in enumerate(query_list):
        input_map[index] = data

    query_map = {}
    if include_predict_signature:
        query_map['signature_name'] = "predict"
    query_map['instances'] = [input_map]

    json_string = json.dumps(query_map)
    print(json_string)
    return json_string


jsonString = convert_query_to_bow("select * from posts where postid=4; SLEEP(5);")

r = requests.post('http://localhost:8501/v1/models/sqlml-estimator:predict', data=jsonString)
r.json()
