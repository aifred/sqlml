#!/usr/bin/env bash

# Creating the model as a container
docker run --rm -v "/Users/Aifred/Google Drive/DatabaseProtection/SqlML/models":/models -e MODEL_NAME='sqlml-estimator' -e MODH='/models/sqlml-estimator' -p 8500:8500 --name tensorflow-server tensorflow/serving

# To get Container IP
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' tensorflow-server